const UUID = "statlabel@smondet";

const St = imports.gi.St;
const Lang = imports.lang;
const PopupMenu = imports.ui.popupMenu;
const Main = imports.ui.main;
const GLib = imports.gi.GLib;
const Util = imports.misc.util;
const Mainloop = imports.mainloop;
const Applet = imports.ui.applet;
const Settings = imports.ui.settings;
const MessageTray = imports.ui.messageTray;

// cf http://billauer.co.il/blog/2018/12/writing-cinnamon-applet/
function backtick(command) {
  try {
    let [result, stdout, stderr] = GLib.spawn_command_line_sync(command);
    if (stdout != null) {
      return stdout.toString();
    }
  }
  catch (e) {
    global.logError(e);
  }

  return "ERROR";
}

function _(str) {
    return str;
}

function MyApplet(metadata, orientation, panel_height, instance_id) {
    this._init(metadata, orientation, panel_height, instance_id);
}

MyApplet.prototype = {
    __proto__: Applet.TextIconApplet.prototype,

    _init: function(metadata, orientation, panel_height, instance_id) {
        Applet.TextIconApplet.prototype._init.call(this, orientation, panel_height, instance_id);
        try {
            this.setAllowedLayout(Applet.AllowedLayout.BOTH);
            //        this.menuManager = new PopupMenu.PopupMenuManager(this);
//
//        this.menu = new Applet.AppletPopupMenu(this, orientation);
//        this.menuManager.addMenu(this.menu);
//
//        this.graph = new St.DrawingArea({reactive: false});
//        this.graph.connect('repaint', Lang.bind(this, this._drawGraph));
//        this.menu.addActor(this.graph); //TODO: Remove top/bottom margin
//        this.graphData = {}
  //      this.menu.connect('open-state-changed', Lang.bind(this, function(menu, isOpen) {
  //          if (isOpen) {
  //              this.graphData = {};
  //              this._loadHistory(this.graph_unit, this.graph_length, this._parseGraphJSON);
  //          }
  //      }));
    //    this.notificationSource = new MessageTray.SystemNotificationSource();
    //    Main.messageTray.add(this.notificationSource);
        this.set_applet_label("WIP");
            this._update_value();
        }
        catch (e) {
            global.logError(e);
        }
    },

    _update_value: function () {
        //this._loadTrackers(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=${this.ticker}&tsyms=${this.currency}`);
        if (this._updateTimeout) {
            Mainloop.source_remove(this._updateTimeout);
        }

        this._updateTimeout = Mainloop.timeout_add(1000, Lang.bind(this, this._update_value));

        let content = backtick("sh -c 'cat $HOME/.uuu-general | tr -d \"\\n\"' ") // | tr -d '\n'
        this.set_applet_label(`■ ${content} ■`);
    },


    on_applet_removed_from_panel: function() {

        if (this._updateTimeout) {
            Mainloop.source_remove(this._updateTimeout);
            this._updateTimeout = undefined;
        }

    },

    on_applet_clicked: function(event) {
        this.menu.toggle();
    }
};

function main(metadata, orientation, panel_height, instance_id) {
    return new MyApplet(metadata, orientation, panel_height, instance_id);
}
